var music = {

    key: "86d2fc6c87382c82f04d5a9679470ad7",
    user: "Brand42",
    url: "http://ws.audioscrobbler.com/2.0/?api_key=",
    getAlbum: "Album.getInfo",
    getArtist: "Artist.getInfo&artist=",
    getTrack: "Track.getInfo",
    albumInfoGot: false,
    artistInfoGot: false,
    getCurrentTrack: "user.getrecenttracks&limit=2",
    trackData: [],
    nowPlayingTrack: '',
    refresh: true,

    ajaxRequest: function(method, callback, successCallBack) {
        var request = new XMLHttpRequest();
        request.open('GET', music.url + music.key + "&user=" + music.user + "&format=json&method=" + method, true);

        request.onload = function() {
          if (request.status >= 200 && request.status < 400) {
            // Success!
            var data = JSON.parse(request.responseText);
            callback(data); // Calls the callback function to process the data
            if(successCallBack) successCallBack = true; // Changes the variable to be true
          } else {
            console.log('Server Error, please try again later');
          }
        };
        request.onerror = function() {
          // There was a connection error of some sort
          console.log('XHR Error');
        };
        request.send();
    },

    // getTheCurrentTrack();
    trackInfo: function(data) {
        var trackInfo = data.recenttracks.track[0];

        if(music.nowPlaying === trackInfo.name) {
            music.refresh = false;
            return false; //check this later
        } else {
            music.refresh = true;
            music.nowPlaying = trackInfo.name
        }

        music.trackData['track'] = trackInfo.name;
        music.trackData['artist'] = trackInfo.artist['#text'];
        music.trackData['album'] = trackInfo.album['#text'];
        music.trackData['artistAlbum'] = trackInfo.artist['#text'] + ' ' + trackInfo.album['#text'];

        // Make artist and album requests from track information
        var artistApiMehod = music.getArtist + escape(trackInfo.artist['#text']);
        var albumApiMethod = music.getAlbum + '&artist=' + escape(trackInfo.artist['#text']) + '&album=' + escape(music.trackData['album']);
        music.ajaxRequest(albumApiMethod, music.albumInfo, music.albumInfoGot);
        music.ajaxRequest(artistApiMehod, music.artistInfo, music.artistInfoGot);
    },

    artistInfo: function(data) {
        music.trackData['artistImage'] = data.artist.image[4]['#text'];
        music.artistInfoGot = true;
        if (music.albumInfoGot) {
            music.init();
        }
    },

    albumInfo: function(data) {
        music.trackData['albumImage'] = data.album.image[4]['#text'];
        music.albumInfoGot = true;
        if (music.artistInfoGot) {
            music.init();
        }
    },

    pythonTime: function(add, remove) {
        if(add) document.body.classList.add('put-the-foot-down');
        if(remove) document.body.classList.remove('put-the-foot-down');
    },

    template: function(obj) {
        if(music.refresh) {
            music.pythonTime(true, false);
            setTimeout(function() {
                document.getElementsByClassName('holder')[0].innerHTML = '';
                var track = obj.track;
                var template = document.getElementById('trackTemplate'),
                    temp = template.innerHTML,
                    objLength = parseInt(Object.keys(obj).length),
                    i = 0,
                    holder = document.getElementsByClassName("holder")[0];

                for(var trackItem in obj) {
                    i++
                    temp = temp.replace('{{' + trackItem + '}}', obj[trackItem]);

                    if(i === objLength) { //Finished the loop
                        holder.innerHTML = temp;
                    }
                }
                music.pythonTime(false, true);
            },1500);
        } else {
            return true;
        }
    },

    init: function() {
        var theData = music.trackData;
        music.template(theData);
        music.albumInfoGot = false;
        music.artistInfoGot = false;
    }

}

music.ajaxRequest(music.getCurrentTrack, music.trackInfo);
music.pythonTime();
setInterval(function() {
    music.ajaxRequest(music.getCurrentTrack, music.trackInfo);
}, 5000);

